package com.FastAndFoodius.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import com.FastAndFoodius.controller.CalculateOrderTotal;

/**
 * Servlet implementation class MenuController
 */
@WebServlet("/Practise_MenuController")
public class Practise_MenuController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Practise_MenuController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CalculateOrderTotal prices = new CalculateOrderTotal();
		
		prices.setQty_chicken_burger(Double.parseDouble(request.getParameter("item_chicken_burger")));
		prices.setQty_quarter_pounder(Double.parseDouble(request.getParameter("item_quarter_pounder")));
		prices.setQty_half_pounder(Double.parseDouble(request.getParameter("item_half_pounder")));
		
		prices.setQty_regular_chips( Double.parseDouble(request.getParameter("item_regular_chips")));
		prices.setQty_curry_chips(Double.parseDouble(request.getParameter("item_curry_chips")));
		prices.setQty_taco_chips(Double.parseDouble(request.getParameter("item_taco_chips"))); 
		
		prices.setQty_snack_box(Double.parseDouble(request.getParameter("item_snack_box"))); 
		prices.setQty_chicken_goujons(Double.parseDouble(request.getParameter("item_chicken_goujons")));
		prices.setQtychicken_nuggets(Double.parseDouble(request.getParameter("item_chicken_nuggets")));
		doGet(request, response);
		
		double total = 0;
		
		
		total = total + prices.getQty_chicken_burger() * prices.getItem_price_chicken_burger();
		total = total + prices.getQty_quarter_pounder() * prices.getItem_price_quarter_pounder();
		total = total + prices.getQty_half_pounder() * prices.getItem_price_half_pounder();
		
		total = total + prices.getQty_regular_chips() * prices.getItem_price_regular_chips();
		total = total + prices.getQty_curry_chips() * prices.getItem_price_curry_chips();
		total = total + prices.getQty_taco_chips() * prices.getItem_price_taco_chips();
		
		total = total + prices.getQty_snack_box() * prices.getItem_price_snack_box();
		total = total + prices.getQty_chicken_goujons() * prices.getItem_price_chicken_goujons();
		total = total + prices.getQtychicken_nuggets() * prices.getItem_price_chicken_nuggets();

		prices.setTotal(total);
		
		request.setAttribute("checkout_total", prices);
		request.getRequestDispatcher("MenuResponse.jsp").forward(request, response);
	}

}
