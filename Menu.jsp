<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Place your order</title>
</head>
<body>

<h1>Please select your order items below!<br></h1>


	<form action="MenuResponse.jsp" method="POST" id="checklist">	
		<div id="burgersdiv">
			<h3><strong>Burgers</strong><br></h3>
			<input type="checkbox" name="burgers_ch" value="chicken burger">Chicken burger<br>
			<input type="checkbox" name="burgers_qp" value="quarter pounder">Quarter Pounder<br>
			<input type="checkbox" name="burgers_hp" value="half pounder">Half Pounder<br>
		</div>
		
		<div id="chipsdiv">
			<h3><strong>Chips</strong><br></h3>
			<input type="checkbox" name="chips_reg" value="regular chips">Regular chips<br>
			<input type="checkbox" name="chips_cur" value="curry chips">Curry chips<br>
			<input type="checkbox" name="chips_tac" value="taco chips">Taco chips<br>
		</div>
		
		<div id="chickendiv">
			<h3><strong>Chicken</strong><br></h3>
			<input type="checkbox" name="chicken_sna" value="snack box">Snack box<br>
			<input type="checkbox" name="chicken_spe" value="chicken special">Chicken special<br>
			<input type="checkbox" name="chicken_hal" value="half chicken">Half chicken<br>
		</div>
		
		<div id="submitdiv">
			<br>
			<input type="submit" value="submit">
		</div>
		
		
	</form>
	


</body>
</html>

